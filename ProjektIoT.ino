#include <ESP8266WiFi.h>
#include <PubSubClient.h>

//MQTT Message
#define MSG_BUFFER_SIZE  (50)

//Wi-Fi
const char* ssid = "Macut";
const char* password = "loralora";
const char* mqtt_server = "10.104.33.229";

//HC-SR04
#define echoPin 4
#define trigPin 5
long duration;
int distance;

//Global Variables
int chk;
char poruka[MSG_BUFFER_SIZE];
String por;
bool control = false;

//MQTT
WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
char msg[MSG_BUFFER_SIZE];
char distMsg[MSG_BUFFER_SIZE];
int value = 0;

//Functions
void setup_wifi() {
  delay(10);
  //We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  por="";
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    por+=(char)payload[i];
  }
  
  if(por=="on" || por=="ON"){
    control = true;
  }else{
    control = false;
  }
  Serial.println();
  Serial.print("Control: ");
  Serial.println(control);
  Serial.println(por);
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("distance", "Spojen arduino");
      // ... and resubscribe
      client.subscribe("control");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

//inicijalizacija
void setup() {
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(13, OUTPUT);
  pinMode(14, OUTPUT);
  Serial.begin(9600);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

// the loop function runs over and over again forever
void loop() {
  delay(1000);
  
  if(control){
    digitalWrite(14, HIGH);
  }else{
    digitalWrite(14, LOW);
  }
                       

  digitalWrite(13, HIGH);
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);

  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  duration = pulseIn(echoPin, HIGH);

  distance = duration * 0.034 / 2;

  Serial.print("Distance: ");
  Serial.print(distance);
  Serial.print(" cm");

  delay(1000);
  digitalWrite(13, LOW);

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  unsigned long now = millis();
  if (now - lastMsg > 2000) {
    lastMsg = now;
    ++value;
    snprintf (msg, MSG_BUFFER_SIZE, "Message #%ld: Distance = %d cm", value, distance);
    snprintf (distMsg, MSG_BUFFER_SIZE, "%d", distance);
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish("messages", msg);
    client.publish("distance", distMsg);
  }
}
